<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-present Qualiteam software Ltd <info@x-cart.com>         |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: https://www.x-cart.com/license-agreement-classic.html |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Functions for the Mailchimp subscription
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-present Qualiteam software Ltd <info@x-cart.com>
 * @license    https://www.x-cart.com/license-agreement-classic.html X-Cart license agreement
 * @version    0b0633768559e57d1124488556a9dbf71d865723, v33 (xcart_4_7_7), 2017-01-23 20:12:10, func.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if (!defined('XCART_START')) {
    header('Location: ../');
    die('Access denied');
}

function func_adv_mailchimp_init() {}

/**
 * Return the object of lib  class by API key
 * https://github.com/drewm/mailchimp-api
 *
 * @param string
 *
 * @return object or error_string
 * @see    ____func_see____
 * @since  1.0.0
 */
function func_adv_mailchimp_get_obj_MCAPI($_apikey = false)
{//{{{
    global $xcart_dir, $top_message, $HTTP_REFERER, $config;

    $_apikey = $_apikey ?: $config['Adv_Mailchimp_Subscription']['adv_mailchimp_apikey'];

    static $inst = array();
    if (isset($inst[$_apikey])) {
        return $inst[$_apikey];
    }
    // require $xcart_dir . '/modules/Adv_Mailchimp_Subscription/lib/autoload.php'; //is not used for performance reason

    require $xcart_dir . '/modules/Adv_Mailchimp_Subscription/lib/drewm/mailchimp-api/src/MailChimp.php';

    try {
        $inst[$_apikey] = new \DrewM\MailChimp\MailChimp($_apikey);
    } catch(Exception $e) {
        x_session_register('top_message');
        $top_message['content'] = 'Adv_Mailchimp_Subscription:' . $e->getMessage();
        $top_message['type'] = 'E';
        func_header_location(func_header_location(func_is_internal_url($HTTP_REFERER) ? $HTTP_REFERER : $_SERVER['PHP_SELF']));
    }

    return $inst[$_apikey];
}//}}}

/**
 * Subscription wrapper for Mailchimp service  (lists method)
 *
 * @param mixed  $listid        id of Mailchimp account
 * @param mixed  $apikey        apikey of Mailchimp account
 *
 * @return array
 * @see    ____func_see____
 * @since  1.0.0
 */
function func_mailchimp_get_lists($listid = false, $apikey = false)
{//{{{

    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI($apikey);

    $mailchimp_return = $mailchimp_api->get('lists');
    if ($mailchimp_api->getLastError()) {
        $mailchimp_return['Error_message'] = $mailchimp_api->getLastError();
        return $mailchimp_return;
    }

    $mailchimp_return['data'] = empty($mailchimp_return['lists']) ? array() : $mailchimp_return['lists'];
    if (!empty($mailchimp_return['lists'])) {
        $mailchimp_return['data'] = $mailchimp_return['lists'];
        unset($mailchimp_return['lists']);
    } else {
        $mailchimp_return = array();
    }
    return $mailchimp_return;
}//}}}

function func_adv_mailchimp_get_campaigns($in_campaignid = false, $fields = array())
{//{{{
    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();

    if (empty($in_campaignid)) {
        $mailchimp_return = $mailchimp_api->get('campaigns');
    } else {
        $mailchimp_return = $mailchimp_api->get("campaigns/$in_campaignid", $fields);
    }

    if ($mailchimp_api->getLastError()) {
        $mailchimp_return = array();
        $mailchimp_return['Error_message'] = $mailchimp_api->getLastError();
    }

    return isset($mailchimp_return['campaigns']) ? $mailchimp_return['campaigns'] : $mailchimp_return;
}//}}}

function func_mailchimp_get_list_by_email($email, $apikey = false)
{//{{{
    global $mailchimp_lists_by_email;
    x_session_register('mailchimp_lists_by_email', array());

    if (isset($mailchimp_lists_by_email[$email]) && empty($mailchimp_lists_by_email[$email]['lock_cache'])) {
        return $mailchimp_lists_by_email[$email]['data'];
    }

    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI($apikey);
    $mailchimp_return = array();

    if (!empty($email)){
        $mailchimp_res = $mailchimp_api->get('lists', array('email' => $email, 'fields' => 'lists.id'));
        if ($mailchimp_api->getLastError() || empty($mailchimp_res['lists'])) {
            $mailchimp_return = array();
        } else {
            foreach ($mailchimp_res['lists'] as $list) {
                $mailchimp_return[] = $list['id'];
            }
        }
    }

    $mailchimp_lists_by_email[$email]['data'] = $mailchimp_return;
    return $mailchimp_return;
}//}}}

function func_mailchimp_lock_session_cache()
{//{{{
    global $mailchimp_lists_by_email;
    x_session_register('mailchimp_lists_by_email');
    $args = func_get_args();
    if (empty($args)) {
        return false;
    }

    foreach ($args as $email) {
        if (isset($mailchimp_lists_by_email[$email])) {
            $mailchimp_lists_by_email[$email] = array('lock_cache' => true);
        }
    }
}//}}}

function func_adv_mailchimp_subscribe($email_address, $user_info, $listid = false,  $apikey = false, $send_confirm_email = false) {
    global $config;

    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI($apikey);

    $mailchimp_merge_vars = $user_info;
    $mailchimp_response = array();

    $mailchimp_return = $mailchimp_api->post("lists/$listid/members",
        array(
            'status' => ($send_confirm_email ? 'pending' : 'subscribed'),
            'email_address' => $email_address,
            'merge_fields' => $mailchimp_merge_vars)
    );

    if (
        !empty($mailchimp_return['status'])
        && $mailchimp_return['status'] == 400
        && $mailchimp_return['title'] == 'Member Exists'
        && $config['Adv_Mailchimp_Subscription']['adv_mailchimp_register_opt'] == 'Y'
    ) {
        $subscriber_hash = $mailchimp_api->subscriberHash($email_address);

        $status = $mailchimp_api->get("lists/$listid/members/$subscriber_hash", array('fields' => 'status'));

        if (
            !empty($status)
            && $status['status'] == 'pending'
        ) {
            //resend confirmation email
            $mailchimp_return = $mailchimp_api->put("lists/$listid/members/$subscriber_hash",
                array(
                    'status' => ($send_confirm_email ? 'pending' : 'subscribed'),
                    'status_if_new' => ($send_confirm_email ? 'pending' : 'subscribed'),
                    'email_address' => $email_address,
                    'merge_fields' => $mailchimp_merge_vars)
            );
        }

    }

    if ($mailchimp_api->getLastError()) {
        $mailchimp_response['Error_message'] = $mailchimp_api->getLastError();
    } else {
        func_mailchimp_lock_session_cache($email_address);
        $mailchimp_response['Response'] = $mailchimp_return;
    }

    return $mailchimp_response;
}

function func_mailchimp_update($email_address, $listid, $mailchimp_updates, $apikey = false)
{//{{{
    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI($apikey);

    $mailchimp_merge_vars = $mailchimp_updates;

    $subscriber_hash = $mailchimp_api->subscriberHash($email_address);
    $mailchimp_return = $mailchimp_api->patch("lists/$listid/members/$subscriber_hash", array('merge_fields' => $mailchimp_merge_vars));

    $mailchimp_response = array();
    if ($mailchimp_api->getLastError()) {
        $mailchimp_response['Error_message'] = $mailchimp_api->getLastError();
    } else {
        if (isset($mailchimp_merge_vars['status'])) {
            func_mailchimp_lock_session_cache($email_address);
        }
        $mailchimp_response['Response'] = $mailchimp_return;
    }

    return $mailchimp_response;
}//}}}

function func_mailchimp_unsubscribe($email_address, $listid = false, $apikey = false)
{
    $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI($apikey);

    $mailchimp_response = array();

    $subscriber_hash = $mailchimp_api->subscriberHash($email_address);
    $mailchimp_return = $mailchimp_api->patch("lists/$listid/members/$subscriber_hash", array('status' => 'unsubscribed'));

    if ($mailchimp_api->getLastError()) {
        $mailchimp_response['Error_message'] = $mailchimp_api->getLastError();
    } else {
        func_mailchimp_lock_session_cache($email_address);
        $mailchimp_response['Response'] = $mailchimp_return;
    }

    return $mailchimp_response;
}

class XCMailChimpEcomm {
    const ID_LIMIT_LENGTH = 50;//http://developer.mailchimp.com/documentation/mailchimp/guides/getting-started-with-ecommerce/#using-the-api
    const CAMPAIGN_EXPIRE_SQL = 7776000;//86400 * 30 * 3
    const PRODUCTS_EXPIRE_SQL = 15768000;//86400 * 365/2

    public static function getStoreByCampaignId($in_campaign_code)
    {//{{{
        global $sql_tbl;
        static $res;

        if (!empty($res)) {
            return $res;
        }

        $store = $res = func_query_first("SELECT store_code,internal_store_id FROM $sql_tbl[mailchimp_campaigns_stores] WHERE campaign_code='" . addslashes($in_campaign_code) . "'") ?: array();
        if (!empty($store)) {
            if (!empty($store['store_code'])) {
                return $store;
            } else {
                return false;
            }
        }

        $list_from_campaign = XCMailChimpEcomm::getListIdByCampaignId($in_campaign_code);
        if (empty($list_from_campaign)) {
            // raw with empty store_code to support cache
            db_query("DELETE FROM $sql_tbl[mailchimp_campaigns_stores] WHERE expire<" . XC_TIME);
            db_query("INSERT IGNORE INTO $sql_tbl[mailchimp_campaigns_stores] SET campaign_code='" . addslashes($in_campaign_code) . "', expire='" . (XC_TIME+86400) . "'");
            return false;
        }

        $store_code = $store['store_code'] = static::getStoreIdByListId($list_from_campaign['list_id']) ?: XCMailChimpEcomm::addStore($list_from_campaign);

        if (!empty($store_code)) {
            $expiry = XC_TIME+static::CAMPAIGN_EXPIRE_SQL;
            db_query("DELETE FROM $sql_tbl[mailchimp_campaigns_stores] WHERE expire<" . XC_TIME);
            db_query("INSERT INTO $sql_tbl[mailchimp_campaigns_stores] SET campaign_code='" . addslashes($in_campaign_code) . "', store_code='" . addslashes($store_code) . "', expire='$expiry' ON DUPLICATE KEY UPDATE expire='$expiry'");
            $store['internal_store_id'] = db_insert_id() ?: func_query_first_cell("SELECT internal_store_id FROM $sql_tbl[mailchimp_campaigns_stores] WHERE campaign_code='" . addslashes($in_campaign_code) . "'");
        }

        return !empty($store_code) ? $store : array();
    }//}}}

    public static function addStore($list)
    {//{{{
        global $xcart_http_host, $config;

        $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();
        $params = array(
            'id' => substr($list['list_id'] . static::getStoreSuffix(), 0, static::ID_LIMIT_LENGTH),
            'list_id' => $list['list_id'],
            'name' => 'Store ' . $xcart_http_host . ' for list ' . $list['list_id'] . ' ' . (empty($list['list_name']) ? '' : $list['list_name']),
            'currency_code' => $config['Adv_Mailchimp_Subscription']['adv_mailchimp_currency_code'],
        );
        $mailchimp_return = $mailchimp_api->post("ecommerce/stores", $params);

        return empty($mailchimp_return['id']) ? false : $mailchimp_return['id'];
    }//}}}

    public static function getOrderCustomer($userinfo)
    { //{{{
        global $config;
        $data = array(
            'id'    => !empty($userinfo['userid']) ? $userinfo['userid'] : md5($userinfo['email']),
            'email_address' => !empty($userinfo['email']) ? $userinfo['email'] : '',
            'first_name' => !empty($userinfo['firstname']) ? $userinfo['firstname'] : (!empty($userinfo['s_firstname']) ? $userinfo['s_firstname'] : ''),
            'last_name' => !empty($userinfo['lastname']) ? $userinfo['lastname'] : (!empty($userinfo['s_lastname']) ? $userinfo['s_lastname'] : ''),
            'company' => !empty($userinfo['company']) ? $userinfo['company'] : '',
            'opt_in_status' => $config['Adv_Mailchimp_Subscription']['adv_mailchimp_subscribe_order'] == 'Y',
            'address' => array(
                'address1' => !empty($userinfo['s_address']) ? $userinfo['s_address'] : '',
                'city' => !empty($userinfo['s_city']) ? $userinfo['s_city'] : '',
                'province' => !empty($userinfo['s_statename']) ? $userinfo['s_statename'] : '',
                'province_code' => !empty($userinfo['s_state']) ? $userinfo['s_state'] : '',
                'postal_code' => !empty($userinfo['s_zipcode']) ? $userinfo['s_zipcode'] : '',
                'country' => !empty($userinfo['s_countryname']) ? $userinfo['s_countryname'] : '',
                'country_code' => !empty($userinfo['s_country']) ? $userinfo['s_country'] : '',
            ),
        );

        return $data;
    }//}}}

    public static function getOrderAddress($order, $in_prefix)
    { //{{{
        $data = array(
            'address1' => !empty($order[$in_prefix . 'address']) ? $order[$in_prefix . 'address'] : '',
            'city' => !empty($order[$in_prefix . 'city']) ? $order[$in_prefix . 'city'] : '',
            'province' => !empty($order[$in_prefix . 'statename']) ? $order[$in_prefix . 'statename'] : '',
            'province_code' => !empty($order[$in_prefix . 'state']) ? $order[$in_prefix . 'state'] : '',
            'postal_code' => !empty($order[$in_prefix . 'zipcode']) ? $order[$in_prefix . 'zipcode'] : '',
            'country' => !empty($order[$in_prefix . 'countryname']) ? $order[$in_prefix . 'countryname'] : '',
            'country_code' => !empty($order[$in_prefix . 'country']) ? $order[$in_prefix . 'country'] : '',
            'phone' => !empty($order[$in_prefix . 'phone']) ? $order[$in_prefix . 'phone'] : '',
        );

        return $data;
    }//}}}

    public static function getLineItems($order_data)
    { //{{{
        $items = array();

        if (empty($order_data['products'])) {
            return array();
        }

        foreach ($order_data['products'] as $k => $pr) {
            $items[] = array(
                'id' => !isset($pr['itemid']) ? ('cart_item' . $k) : ('order_item' . $pr['itemid']),
                'product_id' => static::getProductId($pr),
                'product_variant_id' => static::getProductVariantId($pr),
                'quantity' => intval($pr['amount']),
                'price' => empty($pr['taxed_price']) ? $pr['price'] : $pr['taxed_price'],
            );
        }

        return $items;
    }//}}}

    public static function sendCart($cart, $in_campaignid)
    { //{{{
        global $config, $mailchimp_carts, $current_location, $logged_userid, $login, $user_account;

        if (
            empty($cart['products'])
            || $config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_carts'] != 'Y'
            || empty($in_campaignid)
            || empty($cart['products'])
        ) {
            return false;
        }


        $store = XCMailChimpEcomm::getStoreByCampaignId($in_campaignid);

        if (empty($store['store_code'])) {
            return false;
        }

        //The same call as from Include/Checkout_init.php to use static cache
        x_load('user');
        x_session_register('logged_userid');
        x_session_register('login');
        $userinfo = func_userinfo($logged_userid, !empty($login) ? $user_account['usertype'] : '', false, false, 'H');

        if (empty($userinfo['email'])) {
            return false;
        }

        $cart2send = array(
            'customer' => XCMailChimpEcomm::getOrderCustomer($userinfo),
            'campaign_code' => $in_campaignid,
            'checkout_url' => $current_location . DIR_CUSTOMER . '/cart.php',
            'currency_code' => $config['Adv_Mailchimp_Subscription']['adv_mailchimp_currency_code'],
            'order_total' => $cart['total_cost'],
            'tax_total'         => empty($cart['tax_cost']) ? 0 : $cart['tax_cost'],
            'shipping_total'    => empty($cart['shipping_cost']) ? 0 : $cart['shipping_cost'],
            'lines' => XCMailChimpEcomm::getLineItems($cart),
        );

        $cart2send['id'] = $current_cart_id = md5(serialize($cart2send));

        if (!isset($mailchimp_carts[$current_cart_id])) {
            $_operations = array();
            // remove obsolete carts
            x_session_register('mailchimp_carts', array());
            foreach ($mailchimp_carts as $session_cart_id => $val) {
                $_operations[] = array(
                    'method' => 'DELETE',
                    'path' => "ecommerce/stores/$store[store_code]/carts/$session_cart_id",
                );
                unset($mailchimp_carts[$session_cart_id]) ;
            }

            $mailchimp_carts[$current_cart_id] = true;
            $_operations[] = array(
                'method' => 'POST',
                'path' => "ecommerce/stores/$store[store_code]/carts",
                'body' => json_encode($cart2send),
            );
        }

        if (empty($_operations)) {
            return false;
        }

        $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();

        if (defined('XC_MAILCHIMP_DELETE_BATCHES')) {//{{{
            $all_batches = $mailchimp_api->get('batches', array('fields' => 'batches.id'));
            if (!empty($all_batches['batches'])) {
                foreach ($all_batches['batches'] as $batch) {
                    $res = $mailchimp_api->delete('batches/' . $batch['id']);
                }
            }
        }//}}}

        $mailchimp_return = $mailchimp_api->post('batches', json_encode(array('operations'=>$_operations)));
        return true;
    }//}}}

    public static function sendCartProducts($cart_products, $in_campaignid)
    { //{{{
        global $config, $sql_tbl;

        if (empty($cart_products)) {
            return false;
        }

        $store = XCMailChimpEcomm::getStoreByCampaignId($in_campaignid);

        if (empty($store['store_code'])) {
            return false;
        }

        $search_products_ids = array();
        foreach ($cart_products as $pr) {
            $search_products_ids[] = static::getProductId($pr);
        }
        $store['internal_store_id'] = intval($store['internal_store_id']);
        $already_added_products = func_query_hash("SELECT productid,product_variant_id FROM $sql_tbl[mailchimp_products] WHERE productid IN ('".implode("', '", $search_products_ids)."') AND internal_store_id='$store[internal_store_id]'", 'productid', true, true);

        $_operations = array();
        $replace_str = '';
        foreach ($cart_products as $pr) {
            $productid = static::getProductId($pr);
            $product_variant_id = static::getProductVariantId($pr);
            $is_added = false;

            if (empty($already_added_products[$productid])) {
                $product_image = static::getImage($productid, empty($pr['variantid']) ? 0 : $pr['variantid']);
                // add full product with variant
                $body = array(
                        'id' => $productid,
                        'title' => $pr['product'],
                        'image_url' => $product_image,
                        'variants' => array(
                            'id' => $product_variant_id,
                            'title' => $pr['product'],
                            'sku' => $pr['productcode'],
                            'inventory_quantity' => 10, // As the product is in the cart than it is available for sale
                            'image_url' => $product_image,
                        ),
                );

                if (empty($body['image_url'])) {
                    unset($body['image_url']);
                }

                if (empty($body['variants']['image_url'])) {
                    unset($body['variants']['image_url']);
                }

                $body = str_replace('variants":{"', 'variants":[{"', json_encode($body));
                $body = str_replace('}}', '}]}', $body);
                $_operations[] = array(
                    'method' => 'POST',
                    'path' => "ecommerce/stores/$store[store_code]/products",
                    'body' => $body,
                );

                $is_added = true;
            } elseif (!in_array($product_variant_id, $already_added_products[$productid])) {
                //add variant to existent product
                $body = array(
                    'id' => $product_variant_id,
                    'title' => $pr['product'],
                    'sku' => $pr['productcode'],
                    'inventory_quantity' => 10, // As the product is in the cart than it is available for sale
                    'image_url' => static::getImage($productid, empty($pr['variantid']) ? 0 : $pr['variantid']),
                );

                if (empty($body['image_url'])) {
                    unset($body['image_url']);
                }

                $body = json_encode($body);

                $_operations[] = array(
                    'method' => 'POST',
                    'path' => "ecommerce/stores/$store[store_code]/products/$productid/variants",
                    'body' => $body,
                );
                $is_added = true;
            }

            if (!empty($is_added)) {
                $replace_str .= "('$store[internal_store_id]','" . intval($productid) . "','" . addslashes($product_variant_id) . "', '" . (XC_TIME+static::PRODUCTS_EXPIRE_SQL). "'),";
            }
        }

        if (empty($_operations)) {
            return false;
        }

        $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();

        $operations = json_encode(array('operations'=>$_operations));

        $mailchimp_return = $mailchimp_api->post('batches', $operations);
        if (!empty($replace_str)) {
            if (!mt_rand(0, 10)) {
                db_query("DELETE FROM $sql_tbl[mailchimp_products] WHERE expire<" . XC_TIME);
            }
            db_query("REPLACE INTO $sql_tbl[mailchimp_products] (internal_store_id,productid,product_variant_id,`expire`) VALUES " . trim($replace_str, ','));
        }

        return true;
    }//}}}

    protected static function getListIdByCampaignId($in_campaignid)
    {//{{{
        $res = func_adv_mailchimp_get_campaigns($in_campaignid, array('fields' => 'recipients.list_id,recipients.list_name'));
        return !empty($res['recipients']['list_id']) ? $res['recipients'] : false;
    }//}}}

    protected static function getStoreIdByListId($listid)
    {//{{{
        $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();
        $mailchimp_return = $mailchimp_api->get('ecommerce/stores', array('fields' => 'stores.id,stores.list_id'));

        $store_code = false;
        if (!empty($mailchimp_return['stores'])) {
            foreach ($mailchimp_return['stores'] as $store) {
                if ($store['list_id'] == $listid) {
                    if (defined('DEVELOPMENT_MODE') && strpos($store['id'], ' ') !== false) {
                        continue;
                    }
                    $store_code = $store['id'];
                    if (strpos($store['id'], static::getStoreSuffix()) !== false) {
                        $store_matched_by_domain = $store['id'];
                        break;
                    }
                }
            }
        }

        return empty($store_matched_by_domain) ? $store_code : $store_matched_by_domain;
    }//}}}

    protected static function getStoreSuffix()
    {//{{{
        global $xcart_http_host;
        return '_' . md5($xcart_http_host);
    }//}}}

    protected static function getProductId($product)
    {//{{{
        return $product['productid'];
    }//}}}

    protected static function getProductVariantId($product)
    {//{{{
        return empty($product['variantid']) ? $product['productid'] : $product['productcode'];
    }//}}}

    protected static function getImage($productid, $variantid)
    {//{{{
        global $config, $xcart_http_host;

        if ($config['Adv_Mailchimp_Subscription']['adv_mailchimp_p_recommendations'] != 'Y') {
            return '';
        }

        $image_ids = array(
            'P' => $productid,
            'T' => $productid,
        );

        if (!empty($variantid)) {
            $image_ids['W'] = $variantid;
        }

        $images = func_get_image_url_by_types($image_ids, empty($variantid) ? 'P' : 'W');
        $image_url = '';

        if (is_array($images) && is_array($images['images'])) {
            foreach (array('W', 'P', 'T') as $type) {
                if (isset($images['images'][$type])) {
                    $image = $images['images'][$type];
                    if (is_array($image) && empty($image['is_default'])) {
                        $image_url = $image['url'];
                        break;
                    }
                }

            }
        }

        if (defined('XC_NGROK_PROXY')) {
            $image_url = str_replace($xcart_http_host, XC_NGROK_PROXY, $image_url);
        }
        return $image_url;
    }//}}}
}

function func_mailchimp_adv_campaign_commission($orderid)
{//{{{
    global $mailchimp_campaignid, $config, $mailchimp_carts;

    if (
        defined('XC_MAILCHIMP_CAMPAIGNID')
        && empty($mailchimp_campaignid)
    ) {
        $mailchimp_campaignid = XC_MAILCHIMP_CAMPAIGNID;
    }

    if (empty($mailchimp_campaignid)) {
        return false;
    }

    if ($config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_orders'] == 'Y') {
        $store = XCMailChimpEcomm::getStoreByCampaignId($mailchimp_campaignid);

        if (empty($store['store_code'])) {
            return false;
        }

        $order_data = func_order_data($orderid);

        if (empty($order_data['products'])) {
            return false;
        }

        $prefix = defined('XC_MAILCHIMP_DEBUG') && defined('DEVELOPMENT_MODE') ? ('_' . XC_TIME) : '';
        $order2send = json_encode(array(
            'id'          => $order_data['order']['orderid'] . $prefix,
            'customer'    => XCMailChimpEcomm::getOrderCustomer($order_data['userinfo']),
            'campaign_code' => $mailchimp_campaignid,
            'currency_code' => $config['Adv_Mailchimp_Subscription']['adv_mailchimp_currency_code'],
            'order_total' => $order_data['order']['total'],
            'tax_total'         => empty($order_data['order']['tax']) ? 0 : $order_data['order']['tax'],
            'shipping_total'    => empty($order_data['order']['shipping_cost']) ? 0 : $order_data['order']['shipping_cost'],
            'updated_at_foreign'  => $order_data['order']['date'],
            'processed_at_foreign' => $order_data['order']['date'],
            'shipping_address' => XCMailChimpEcomm::getOrderAddress($order_data['order'], 's_'),
            'billing_address' => XCMailChimpEcomm::getOrderAddress($order_data['order'], 'b_'),
            'lines'       => XCMailChimpEcomm::getLineItems($order_data),
        ));


        $mailchimp_api = func_adv_mailchimp_get_obj_MCAPI();
        $_operations = array();
        $_operations[] = array(
            'method' => 'POST',
            'path' => "ecommerce/stores/$store[store_code]/orders",
            'body' => $order2send,
        );

        // Delete all session carts
        if ($config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_carts'] == 'Y') {
            x_session_register('mailchimp_carts', array());
            foreach ($mailchimp_carts as $session_cart_id => $val) {
                $_operations[] = array(
                    'method' => 'DELETE',
                    'path' => "ecommerce/stores/$store[store_code]/carts/$session_cart_id",
                );
            }
            $mailchimp_carts = array();
        }

        if (defined('XC_MAILCHIMP_DELETE_BATCHES')) {//{{{
            $all_batches = $mailchimp_api->get('batches', array('fields' => 'batches.id'));
            if (!empty($all_batches['batches'])) {
                foreach ($all_batches['batches'] as $batch) {
                    $res = $mailchimp_api->delete('batches/' . $batch['id']);
                }
            }
        }//}}}

        $mailchimp_return = $mailchimp_api->post('batches', json_encode(array('operations'=>$_operations)));
        return true;
    }
}//}}}

function func_mailchimp_batch_subscribe($userinfo, $_mailchimp_subscription)
{//{{{
    if (!empty($userinfo['email']) && $_mailchimp_subscription) {

        $mailchimp_user_info = array(
            'FNAME' => $userinfo['firstname'],
            'LNAME' => $userinfo['lastname'],
            'email' => $userinfo['email'],
            'phone' => $userinfo['b_phone'],
            'website' => $userinfo['url'],
            'address' => array(
                           'addr1'   => $userinfo['b_address'],
                           'city'    => $userinfo['b_city'],
                           'state'   => $userinfo['b_state'],
                           'zip'     => $userinfo['b_zipcode'],
                           'country' => $userinfo['b_country']
                         )
        );
        foreach ($_mailchimp_subscription as $key => $id) {
            func_adv_mailchimp_subscribe(
                $userinfo['email'],
                $mailchimp_user_info,
                $key,
                false,
                true
            );
        }
    }
}//}}}

function func_mailchimp_resubscribe()
{//{{{
    global $sql_tbl, $config;
    global $firstname, $lastname, $email;
    global $old_userinfo,$mailchimp_subscription,$mc_newslists;

    $mc_newslists = func_query("SELECT mc_list_id FROM $sql_tbl[mailchimp_newslists] WHERE avail='Y'");

    if (!empty($mc_newslists) && !func_is_ajax_request() && $email) {
        $mailchimp_user_info = array(
            'FNAME' => $firstname,
            'LNAME' => $lastname,
            'phone' => $old_userinfo['b_phone'],
            'website' => $old_userinfo['url'],
            'address' => array(
                           'addr1'   => $old_userinfo['b_address'],
                           'city'    => $old_userinfo['b_city'],
                           'state'   => $old_userinfo['b_state'],
                           'zip'     => $old_userinfo['b_zipcode'],
                           'country' => $old_userinfo['b_country']
                         )

        );
        $mc_nls = array();
        foreach($mc_newslists as $mc_lt){
            $mc_nls[] = $mc_lt['mc_list_id'];
        }

        $mailchimp_cur_subs = array();
        $mailchimp_cur_subs = $orig_mailchimp_cur_subs = func_mailchimp_get_list_by_email($old_userinfo['email']);
        $mailchimp_cur_subs = array_intersect($mailchimp_cur_subs,$mc_nls);
        $mailchimp_ext_subs = array();

        if ($email != $old_userinfo['email']) {
            $mailchimp_ext_subs = func_mailchimp_get_list_by_email($email);
        } else {
            $mailchimp_ext_subs = $orig_mailchimp_cur_subs;
        }
        $mailchimp_ext_subs = array_intersect($mailchimp_ext_subs,$mc_nls);

        $mailchimp_subs_keys = array();
        if (is_array($mailchimp_subscription)) {
            $mailchimp_subs_keys = array_keys($mailchimp_subscription);
        }

        $mailchimp_delid = array_diff($mailchimp_cur_subs, $mailchimp_subs_keys);
        $mailchimp_insid = array_diff($mailchimp_subs_keys, $mailchimp_cur_subs,$mailchimp_ext_subs);
        $mailchimp_updid = array_intersect($mailchimp_cur_subs, $mailchimp_subs_keys);
        $mailchimp_updid = array_diff($mailchimp_updid, $mailchimp_ext_subs);

        foreach ($mailchimp_delid as $id) {
            $mailchimp_response = func_mailchimp_unsubscribe($old_userinfo['email'], $id);
        }

        if (
            count($mailchimp_updid) > 0
            && ($old_userinfo['email'] != stripslashes($email) || $old_userinfo['firstname'] != $firstname  )
        ) {
            foreach ($mailchimp_updid as $id) {
                func_mailchimp_update(
                    $old_userinfo['email'],
                    $id,
                    array(
                        'EMAIL' => $email,
                        'FNAME' => $firstname,
                        'LNAME' => $lastname,
                    )
                );
            }
        }

        if ($config['Adv_Mailchimp_Subscription']['adv_mailchimp_register_opt'] == 'Y') {
            $_send_confirm_email = true;
        } else {
            $_send_confirm_email = false;
        }

        foreach ($mailchimp_insid as $id) {
            $mailchimp_response = func_adv_mailchimp_subscribe($email, $mailchimp_user_info, $id, false, $_send_confirm_email);
        }
    }
}//}}}

function func_mailchimp_new_adv_campaign_commission()
{//{{{
    global $mailchimp_campaignid, $REQUEST_METHOD, $mode, $config, $cart;

    if (!empty($_GET['mc_cid'])) {
        x_session_register('mailchimp_campaignid');
        $mailchimp_campaignid = $_GET['mc_cid'];
        $days = $config['Adv_Mailchimp_Subscription']['adv_mailchimp_campaign_expire'];
        $days = empty($days) ? 1 : $days;
        $thirty_days = 60 * 60 * 24 * $days;
        func_setcookie('mailchimp_campaignid', $mailchimp_campaignid, XC_TIME + $thirty_days);
    }

    if (
        defined('XC_MAILCHIMP_CAMPAIGNID')
        && empty($mailchimp_campaignid)
    ) {
        $mailchimp_campaignid = XC_MAILCHIMP_CAMPAIGNID;
    }

    // add cart products to mailchimp
    if (
        strpos($_SERVER['PHP_SELF'], 'cart.php') !== false
        && $REQUEST_METHOD == 'GET'
        && !func_is_ajax_request()
        && (empty($mode) || $mode == 'checkout')
        && !empty($mailchimp_campaignid)
    ) {
        x_session_register('cart');
        if (
            $config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_orders'] == 'Y'
            || $config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_carts'] == 'Y'
        ) {
            XCMailChimpEcomm::sendCartProducts(empty($cart['products']) ? array() : $cart['products'], $mailchimp_campaignid);
        }

        if ($config['Adv_Mailchimp_Subscription']['adv_mailchimp_send_carts'] == 'Y') {
            XCMailChimpEcomm::sendCart($cart, $mailchimp_campaignid);
        }
    }
}//}}}

function func_mailchimp_save_subscription($mailchimp_subscription) {

    global $saved_userinfo, $user;

    if (is_array($mailchimp_subscription)) {
        $saved_userinfo[$user]['mailchimp_subscription'] = $mailchimp_subscription;
    }

}

function func_mailchimp_get_subscription($userinfo)
{//{{{
    global $mailchimp_subscription;
    if (empty($userinfo['mailchimp_subscription'])) {
        $lists_by_email = func_mailchimp_get_list_by_email($userinfo['email']);
        if (!empty($lists_by_email)) {
            $mailchimp_subscription = array();
            foreach ($lists_by_email as $v) {
                $mailchimp_subscription[$v] = true;
            }
        }
   } else {
      $mailchimp_subscription = $userinfo['mailchimp_subscription'];
   }

}//}}}

function func_mailchimp_assign_to_smarty(){

    global $smarty, $sql_tbl, $mailchimp_subscription, $mc_newslists, $shop_language;

    if (isset($mailchimp_subscription)) {
         $smarty->assign('mailchimp_subscription', $mailchimp_subscription);
    }
    $mc_newslists = func_query("SELECT * FROM $sql_tbl[mailchimp_newslists] WHERE avail='Y' AND subscribe='Y' AND lngcode='$shop_language'");
    $smarty->assign('mc_newslists', $mc_newslists);
}

function func_mailchimp_unsubscribe_newslists($email){

    global $sql_tbl, $mc_newslists, $redirect_to;

    $mc_newslists = func_query("SELECT * FROM $sql_tbl[mailchimp_newslists] WHERE avail='Y'");
    if (count($mc_newslists) > 0) {
       foreach ($mc_newslists as $list){
           func_mailchimp_unsubscribe($email, $list['mc_list_id']);
       }
   }
   func_header_location($redirect_to . '/home.php?mode=unsubscribed&email=' . urlencode(stripslashes($email)));
}

function func_mailchimp_adm_get_currencies()
{//{{{
    global $sql_tbl;

    return func_query_hash("SELECT code, CONCAT(name, ' (', code, ')') FROM $sql_tbl[currencies] ORDER BY name", 'code', false, true);
}//}}}
