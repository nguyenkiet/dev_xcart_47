<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-present Qualiteam software Ltd <info@x-cart.com>         |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: https://www.x-cart.com/license-agreement-classic.html |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Checkout by Amazon
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-present Qualiteam software Ltd <info@x-cart.com>
 * @license    https://www.x-cart.com/license-agreement-classic.html X-Cart license agreement
 * @version    0b0633768559e57d1124488556a9dbf71d865723, v27 (xcart_4_7_7), 2017-01-23 20:12:10, func.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 *
 */

if ( !defined('XCART_SESSION_START') ) { header('Location: ../../'); die('Access denied'); }

function func_amazon_pa_debug($message)
{ // {{{

    if (!defined('AMAZON_PA_DEBUG') || empty($message)) {
        return true;
    }

    x_log_add('amazon_pa', $message);
    return true;
} // }}}

function func_amazon_pa_error($message)
{ // {{{

    x_log_add('amazon_pa', $message);
    return true;
} // }}}

/**
 * For PHP compability
 */
if (!function_exists('getallheaders')) { // {{{
    function getallheaders()
    { // {{{
        $headers = '';
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    } // }}}
} // }}}

/*
* dynamically add module patches to customer tpls
*/
function Smarty_prefilter_add_amazon_pa_entry_points($source, $smarty)
{//{{{
    global $xcart_dir;
    static $is_called ;

    if (!empty($is_called)) {
        return $source;
    }
    $is_called = 1;

    require $xcart_dir . XC_DS . 'modules' . XC_DS . 'Amazon_Payments_Advanced' . XC_DS . 'lib' . XC_DS . 'dynamic_tpl_patcher.php';//old_version_compatible; conflict with xcart/modules/XAuth/ext.core.php <=4.7.3
    modules\Amazon_Payments_Advanced\lib\x_tpl_add_callback_patch('modules/One_Page_Checkout/opc_payment.tpl', 'func_amazon_pa_patch_opc_payment_tpl', modules\Amazon_Payments_Advanced\lib\X_TPL_PREFILTER);

    return $source;
}//}}}

function func_amazon_pa_patch_opc_payment_tpl($tpl_name, $tpl_source)
{//{{{
    global $xcart_dir;

    if (strpos($tpl_source, '$payment_standalone}') !== false) {#lintoff
        return $tpl_source;
    }

    // add to the end od skin/common_files/modules/One_Page_Checkout/opc_payment.tpl
    $snippet =<<<EOT
        {if \$payment_standalone}
        {load_defer_code type="css"}
        {load_defer_code type="js"}
        {/if}\n
EOT;

    $tpl_source = preg_replace('%</div>\s*$%s', $snippet . '\\0', $tpl_source);
    #linton

    func_amazon_pa_tpl_debug($tpl_name, $tpl_source);

    return $tpl_source;
}//}}}

function func_amazon_pa_tpl_debug($tpl_name, $tpl_source)
{//{{{
    if (defined('AMAZON_PA_DEBUG')) {
       x_log_add('amazon_pa_patched_files', 'patched_file:' . $tpl_name . "\n" . $tpl_source);
    }
}//}}}

function func_amazon_pa_init()
{ // {{{
    global $smarty, $config;

    // check configuration compability issues
    if (
        func_constant('AREA_TYPE') == 'C'
        && (
            !func_amazon_pa_is_configured()
            || (
                ($amazon_pa_message = func_amazon_pa_has_compability_issues())
                && !empty($amazon_pa_message)
            )
        )
    ) {
        global $active_modules;

        // disable module for customers
        unset($active_modules[AMAZON_PAYMENTS_ADVANCED]);
        $smarty->assign('active_modules', $active_modules);

        return;
    }

    $smarty->assign('amazon_pa_enabled', true);

    if (defined('ADMIN_MODULES_CONTROLLER')) {
        if (function_exists('func_add_event_listener')) {
            func_add_event_listener('module.ajax.toggle', 'func_amazon_pa_on_module_toggle');
        }
    }

    func_amazon_pa_load_classes();

    if (defined('QUICK_START') || func_constant('AREA_TYPE') != 'C') {
        return;
    }

    if (version_compare($config['version'], XC_AMAZON_PA_WITH_ENTRY_POINTS) < 0 && !defined('XC_AMAZON_PA_IS_IN_CORE') && !empty($smarty)) {
        if (version_compare($config['version'], '4.7.0') >= 0) {
            // Smarty_prefilter_add_amazon_pa_entry_points is called here
            $smarty->addAutoloadFilters(array('add_amazon_pa_entry_points'), 'pre');
        } else {
            Smarty_prefilter_add_amazon_pa_entry_points('', null);
        }
    }

    return;
} // }}}

function func_amazon_pa_is_configured()
{ // {{{
    global $config;

    return (
        !empty($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_sid'])
        && !empty($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_access_key'])
        && !empty($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_secret_key'])
        && !empty($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_cid'])
    );
} // }}}

function func_amazon_pa_has_compability_issues()
{ // {{{
    global $config;

    if (empty($config['HTTPS_test_passed'])) {
        x_load('tests');
        list($https_check_success, $https_check_error) = test_https_availibility();
    } else {
        $https_check_success = $config['HTTPS_test_passed'] === 'Y';
    }

    return $https_check_success ? false : (!empty($https_check_error) ? $https_check_error : 'Unknown error');
} // }}}

function func_amazon_pa_on_module_toggle($module_name, $module_new_state)
{ // {{{

    global $sql_tbl, $active_modules;

    if (
        $module_name == AMAZON_PAYMENTS_ADVANCED
        && $module_new_state == true
        && !empty($active_modules['Amazon_Checkout'])
    ) {
        db_query("UPDATE $sql_tbl[modules] SET active = 'N' WHERE module_name = 'Amazon_Checkout'");
        return 'modules.php';
    }
} // }}}

function func_amazon_pa_load_classes()
{ // {{{
    if (!function_exists('func_amazon_feeds_load_class')) {

        function func_amazon_pa_load_class($class_name)
        {
            global $xcart_dir;

            static $classesLibPayWithAmazon = array( // {{{
                'PayWithAmazon\Client' => 'Client',
                'PayWithAmazon\ClientInterface' => 'ClientInterface',
                'PayWithAmazon\HttpCurl' => 'HttpCurl',
                'PayWithAmazon\HttpCurlInterface' => 'HttpCurlInterface',
                'PayWithAmazon\IpnHandler' => 'IpnHandler',
                'PayWithAmazon\IpnHandlerInterface' => 'IpnHandlerInterface',
                'PayWithAmazon\Regions' => 'Regions',
                'PayWithAmazon\ResponseInterface' => 'ResponseInterface',
                'PayWithAmazon\ResponseParser' => 'ResponseParser',
            ); // }}}

            if (
                isset($classesLibPayWithAmazon[$class_name])
            ) {
                include $xcart_dir . XC_DS . 'modules' . XC_DS . AMAZON_PAYMENTS_ADVANCED . XC_DS . 'lib' . XC_DS . 'amazonLAP' . XC_DS . 'PayWithAmazon' . XC_DS . $classesLibPayWithAmazon[$class_name] . '.php';
                return;
            }
        }

        // register class loader
        spl_autoload_register('func_amazon_pa_load_class');
    }
} // }}}

function func_amazon_pa_is_API_in_test_mode()
{ // {{{
    global $config;

    return ($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_mode'] === 'test');
} // }}}

function func_amazon_pa_get_client_API()
{ // {{{
    global $config;

    static $clientAPI = null;

    if (null === $clientAPI) {
        $clientAPI = new PayWithAmazon\Client(array(
            'merchant_id'   => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_sid'],
            'secret_key'    => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_secret_key'],
            'access_key'    => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_access_key'],
            'region'        => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_region'],
            'currency_code' => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_currency'],
            'sandbox'       => func_amazon_pa_is_API_in_test_mode(),
            'platform_id'   => AMAZON_PA_PLATFORM_ID,
            'application_name'    => 'X-Cart',
            'application_version' => $config['version'],
            'client_id'     => $config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_cid'],
        ));
    }

    return $clientAPI;
} // }}}

function func_amazon_pa_get_aorefid_details($aorefid, $aadct)
{ // {{{
    $details = array();

    $amazonAPI = func_amazon_pa_get_client_API();
    $response = $amazonAPI->getOrderReferenceDetails(array(
        'amazon_order_reference_id' => $aorefid,
        'address_consent_token' => $aadct,
    ));

    if (
        ($result = $response->toArray())
        && !empty($result)
        && intval($result['ResponseStatus']) === 200
    ) {
        $details = $result;
    }

    return $details;
} // }}}

function func_amazon_pa_detect_state($state, $country, $zipcode = '')
{ // {{{
    global $sql_tbl;

    $init_state = trim($state);

    if (($state_code = func_query_first_cell("SELECT code FROM $sql_tbl[states] WHERE country_code='$country' AND (state='$init_state' OR code='$init_state')"))) {

        return $state_code;

    } else {

        x_load('user');
        if (($state_code = func_detect_state_by_zipcode($country, $zipcode))) {
            return $state_code;
        }
    }

    return 'Other';
} // }}}

function func_amazon_pa_get_aorefid_billing_address($aorefid, $aadct)
{ // {{{
    $address = array();

    $result = func_amazon_pa_get_aorefid_details($aorefid, $aadct);

    if (!empty($result)) {
        if (
            isset($result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['BillingAddress'])
            && ($destination = $result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['BillingAddress'])
        ) {
            $address['zipcode'] = $destination['PostalCode'];
            $address['country'] = $destination['CountryCode'];
            $address['state'] = func_amazon_pa_detect_state($destination['StateOrRegion'], $destination['CountryCode'], $destination['PostalCode']);
            $address['city'] = $destination['City'];
            $address['phone'] = $destination['Phone'];
            $address['address'] = $destination['AddressLine1'];

            if (isset($destination['AddressLine2'])) {
                $address['address_2'] = $destination['AddressLine2'];
            }

            $list = explode(' ', $destination['Name'], 2);

            $address['firstname'] = isset($list[0]) ? $list[0] : '';
            $address['lastname'] = isset($list[1]) ? $list[1] : '';

            $address = func_prepare_address($address);
        }
    }

    return $address;
} // }}}

function func_amazon_pa_get_aorefid_shipping_address($aorefid, $aadct)
{ // {{{
    $address = array();

    $result = func_amazon_pa_get_aorefid_details($aorefid, $aadct);

    if (!empty($result)) {
        if (
            isset($result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination'])
            && ($destination = $result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Destination']['PhysicalDestination'])
        ) {
            $address['zipcode'] = $destination['PostalCode'];
            $address['country'] = $destination['CountryCode'];
            $address['state'] = func_amazon_pa_detect_state($destination['StateOrRegion'], $destination['CountryCode'], $destination['PostalCode']);
            $address['city'] = $destination['City'];
            $address['phone'] = $destination['Phone'];
            $address['address'] = $destination['AddressLine1'];

            if (isset($destination['AddressLine2'])) {
                $address['address_2'] = $destination['AddressLine2'];
            }

            $list = explode(' ', $destination['Name'], 2);

            $address['firstname'] = isset($list[0]) ? $list[0] : '';
            $address['lastname'] = isset($list[1]) ? $list[1] : '';

            $address = func_prepare_address($address);
        }
    }

    return $address;
} // }}}

function func_amazon_pa_get_aorefid_profile_info($aorefid, $aadct)
{ // {{{
    $profile = array();

    $result = func_amazon_pa_get_aorefid_details($aorefid, $aadct);

    if (!empty($result)) {
        if (
            isset($result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Buyer'])
            && ($buyer = $result['GetOrderReferenceDetailsResult']['OrderReferenceDetails']['Buyer'])
        ) {
            $profile['email'] = $buyer['Email'];

            $list = explode(' ', $buyer['Name'], 2);

            $profile['firstname'] = isset($list[0]) ? $list[0] : '';
            $profile['lastname'] = isset($list[1]) ? $list[1] : '';
        }
    }

    return $profile;
} // }}}

function func_amazon_pa_get_API_seller_notes()
{ // {{{
    global $customer_notes;

    if (!func_amazon_pa_is_API_in_test_mode()) {
        return '';
    }

    return $customer_notes;
} // }}}

function func_amazon_pa_is_API_capture_now()
{ // {{{
    global $config;

    // capture immediate or not
    return ($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_capture_mode'] == 'C');
} // }}}

function func_amazon_pa_is_API_sync_mode()
{ // {{{
    global $config;

    // sync request (returns only "open" or "declined" status, no "pending")
    return ($config[AMAZON_PAYMENTS_ADVANCED]['amazon_pa_sync_mode'] == 'S');
} // }}}

function func_amazon_pa_get_API_transaction_timeout()
{ // {{{
    $defaultTimeout = 1440;

    // sync request (returns only "open" or "declined" status, no "pending")
    return (func_amazon_pa_is_API_sync_mode() ? 0 : $defaultTimeout);
} // }}}

function func_ajax_block_amazon_pa_shipping()
{ // {{{
    global $smarty, $config, $sql_tbl, $active_modules, $xcart_dir;
    global $logged_userid, $login_type, $login, $cart, $userinfo, $is_anonymous, $user_account;
    global $xcart_catalogs, $xcart_catalogs_secure, $current_area;
    global $current_carrier, $shop_language;
    global $intershipper_rates, $intershipper_recalc, $dhl_ext_country_store, $checkout_module, $empty_other_carriers, $empty_ups_carrier, $amazon_enabled, $paymentid, $products;
    global $totals_checksum_init, $totals_checksum;

    if (!defined('ALL_CARRIERS')) {
        define('ALL_CARRIERS', 1);
    }

    x_load(
        'cart',
        'shipping',
        'product',
        'user'
    );

    x_session_register('cart');
    x_session_register('intershipper_rates');
    x_session_register('intershipper_recalc');
    x_session_register('current_carrier');
    x_session_register('dhl_ext_country_store');
    XCAjaxSessions::getInstance()->requestForSessionSave(__FUNCTION__);

    $userinfo = func_userinfo(0, $login_type, false, false, 'H');

    // Prepare the products data
    $products = func_products_in_cart($cart, @$userinfo['membershipid']);

    $intershipper_recalc = 'Y';

    $checkout_module = '';
    include $xcart_dir . '/include/cart_calculate_totals.php';

    $check_smarty_vars = array('dhl_account_used', 'checkout_module', 'is_other_carriers_empty', 'is_ups_carrier_empty', 'need_shipping', 'shipping_calc_error', 'shipping_calc_service', 'main', 'current_carrier', 'show_carriers_selector', 'dhl_ext_countries', 'has_active_dhl_smethods', 'dhl_ext_country');
    func_assign_smarty_vars($check_smarty_vars);
    $smarty->assign('main', 'checkout');
    $smarty->assign('userinfo', $userinfo);

    return func_ajax_trim_div(func_display('modules/One_Page_Checkout/opc_shipping.tpl', $smarty, false));
} // }}}

function func_ajax_block_amazon_pa_totals()
{ // {{{
    global $smarty, $config, $sql_tbl, $active_modules, $xcart_dir;
    global $logged_userid, $login_type, $login, $cart, $userinfo, $is_anonymous, $user_account;
    global $xcart_catalogs, $xcart_catalogs_secure;
    global $current_carrier, $shop_language, $current_area, $checkout_module;
    global $intershipper_rates, $intershipper_recalc, $dhl_ext_country_store, $products;
    global $totals_checksum_init, $totals_checksum;

    if (!defined('ALL_CARRIERS')) {
        define('ALL_CARRIERS', 1);
    }

    x_load(
        'cart',
        'shipping',
        'product',
        'user'
    );

    x_session_register('cart');
    x_session_register('intershipper_rates');
    x_session_register('intershipper_recalc');
    x_session_register('current_carrier');
    x_session_register('dhl_ext_country_store');
    XCAjaxSessions::getInstance()->requestForSessionSave(__FUNCTION__);

    $userinfo = func_userinfo(0, $login_type, false, false, 'H');

    // Prepare the products data
    $products = func_products_in_cart($cart, @$userinfo['membershipid']);

    $intershipper_recalc = 'Y';

    $checkout_module = '';
    include $xcart_dir . '/include/cart_calculate_totals.php';

    $check_smarty_vars = array('zero', 'transaction_query', 'shipping_cost', 'reg_error', 'paid_amount', 'need_shipping', 'minicart_total_items', 'force_change_address', 'paymentid', 'need_alt_currency');
    func_assign_smarty_vars($check_smarty_vars);
    $smarty->assign('main', 'checkout');

    $smarty->assign('userinfo',    $userinfo);
    $smarty->assign('products',    $products);
    $smarty->assign('cart_totals_standalone', true);

    return func_ajax_trim_div(func_display('modules/One_Page_Checkout/summary/cart_totals.tpl', $smarty, false));
} // }}}

function func_amazon_pa_get_payment_tab()
{ // {{{
    global $sql_tbl;

    // get config vars
    global $smarty;
    $configuration = func_query("SELECT * FROM $sql_tbl[config] WHERE category = '" . AMAZON_PAYMENTS_ADVANCED ."' ORDER BY orderby");
    foreach ($configuration as $k => $v) {
        if (in_array($v['type'], array('selector', 'multiselector'))) {
            $vars = func_parse_str(trim($v['variants']), "\n", ":");
            $vars = func_array_map('trim', $vars);
            $configuration[$k]['variants'] = array();

            foreach ($vars as $vk => $vv) {
                if (!empty($vv) && strpos($vv, "_") !== false && strpos($vv, " ") === false) {
                    $name = func_get_langvar_by_name(addslashes($vv), null, false, true);
                    if (!empty($name)) {
                        $vv = $name;
                    }
                }
                $configuration[$k]['variants'][$vk] = array("name" => $vv);
            }

            foreach ($configuration[$k]['variants'] as $vk => $vv) {
                $configuration[$k]['variants'][$vk]['selected'] = $configuration[$k]['type'] == "selector"
                    ? $configuration[$k]['value'] == $vk
                    : in_array($vk, $configuration[$k]['value']);

            }
        }
    }
    $smarty->assign('amazon_pa_configuration', $configuration);

    return  array(
        'title' => func_get_langvar_by_name('lbl_amazon_pa_amazon_advanced'),
        'tpl' => 'modules/Amazon_Payments_Advanced/payment_tab.tpl',
        'anchor' => 'payment-amazon-pa',
    );
} // }}}

function func_amazon_pa_save_order_extra($orderids, $key, $val)
{ // {{{

    if (!is_array($orderids)) {
        $orderids = array($orderids);
    }

    foreach ($orderids as $orderid) {
        func_array2insert(
            'order_extras',
            array(
                'orderid' => $orderid,
                'khash' => $key,
                'value' => $val
            ),
            true
        );
    }
} // }}}

function func_amazon_pa_on_change_order_status($order_data, $status)
{ // {{{

    $order = $order_data['order'];

    if ($status == $order['status']) {
        return;
    }

    if (empty($order['extra']['AmazonOrderReferenceId'])) {
        return; // not amazon order
    }

    $amazonAPI = func_amazon_pa_get_client_API();
    $requestParameters = array(
        'amazon_order_reference_id' => $order['extra']['AmazonOrderReferenceId'],
    );

    if ($status == 'D') {
        // cancel ORO if declined
        $amazonAPI->cancelOrderReference($requestParameters);
    }

    if ($status == 'P') {
        // close ORO when captured
        $amazonAPI->closeOrderReference($requestParameters);
    }
} // }}}
