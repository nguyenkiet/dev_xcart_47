{*
a26546caee6d4cd5438ef532e0f1d5a40f4b83f1, v5 (xcart_4_7_6), 2016-06-10 12:05:52, kount_data.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

<a name="kount"></a>
<div class="xpc-kount-data">

  {if $is_invoice_page}
    <strong>{$lng.lbl_xpc_kount_data}</strong><br />
  {else}
    {include file="main/subheader.tpl" title=$lng.lbl_xpc_kount_data}
  {/if}

  <h3>
    {if $order.extra.xpc_kount_data.Auto eq 'D'}
      {$lng.lbl_xpc_kount_auto_decline}.
    {elseif $order.extra.xpc_kount_data.Auto eq 'R'}
      {$lng.lbl_xpc_kount_auto_review}.
    {elseif $order.extra.xpc_kount_data.Auto eq 'A'}
      {$lng.lbl_xpc_kount_auto_accept}.
    {/if}
    {$lng.lbl_xpc_kount_score}:
    <span class="auto-{$order.extra.xpc_kount_data.Auto|default:"R"}">{$order.extra.xpc_kount_data.Score|default:0}</span>
  </h3>

  <br/>

  <div class="xpc-kount-content">

    {$lng.lbl_xpc_kount_transaction_id}: {$order.extra.xpc_kount_data['Transaction ID']}

    {if $order.extra.xpc_kount_data.Rules}
      <h4>{$lng.lbl_xpc_kount_rules}:</h4>
      <ul>
      {foreach from=$order.extra.xpc_kount_data.Rules item=rule key=k}
        <li>{$rule}</li>
      {/foreach}
      </ul>
    {/if}

    {if $order.extra.xpc_kount_data.Errors}
      <h4>{$lng.lbl_xpc_kount_errors}:</h4>
      <ul>
      {foreach from=$order.extra.xpc_kount_data.Errors item=error key=k}
        <li>{$error}</li>
      {/foreach}
      </ul>
    {/if}

    {if $order.extra.xpc_kount_data.Warnings}
      <h4>{$lng.lbl_xpc_kount_warnings}:</h4>
      <ul>
      {foreach from=$order.extra.xpc_kount_data.Warnings item=warning key=k}
        <li>{$warning}</li>
      {/foreach}
      </ul>
    {/if}

  </div>

</div>
