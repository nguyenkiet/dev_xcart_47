{*
a26546caee6d4cd5438ef532e0f1d5a40f4b83f1, v3 (xcart_4_7_6), 2016-06-10 12:05:52, kount_status.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{if $auto eq 'D'}
  {assign var=hint value=$lng.lbl_xpc_kount_auto_decline}
{elseif $auto eq 'R'}
  {assign var=hint value=$lng.lbl_xpc_kount_auto_review}
{elseif $auto eq 'A'}
  {assign var=hint value=$lng.lbl_xpc_kount_auto_accept}
{/if}

<a class="kount-status auto-{$auto|default:"R"}" href="order.php?orderid={$orderid}#kount" title="{$hint}">&nbsp;</a>
