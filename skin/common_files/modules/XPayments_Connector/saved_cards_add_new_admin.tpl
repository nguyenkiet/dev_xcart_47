{*
fbafb3086edc7e344773b192b8bd89e29ac6b690, v3 (xcart_4_7_6), 2016-06-13 18:33:15, saved_cards_add_new_admin.tpl, random

vim: set ts=2 sw=2 sts=2 et:
*}
{include file="modules/XPayments_Connector/saved_cards_add_new_js.tpl" is_admin=true}

<div id="xpc_iframe_container" style="display: none;">

  {capture name=xpc_save_cc_amount}{currency value=$config.XPayments_Connector.xpc_save_cc_amount}{/capture}
  {$lng.txt_xpc_saved_cards_add_new|substitute:amount:$smarty.capture.xpc_save_cc_amount}
  <br /><br />
  <div id="xpc_iframe_section" style="display: inline-block;">
    <form name="xpc_save_card_form" onsubmit="javascript: return submitXPCFrame();">
    {include file="modules/XPayments_Connector/xpc_iframe.tpl" save_cc=true}
    <div id="xpc_submit" class="button-row">
      <input type="submit" value="{$lng.lbl_submit}" />
    </div>
    </form>
  </div>
  <div id="xpc_address_hint">
    <strong>{$lng.lbl_billing_address}:</strong>
    {include file="customer/main/address_details_html.tpl"}
    <div class="address-line xpc-address-comment">{$lng.txt_xpc_add_new_card_address_admin_note}</div>
  </div>

</div>
