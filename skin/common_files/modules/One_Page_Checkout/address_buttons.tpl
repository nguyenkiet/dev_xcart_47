{*
41562c5cfeb62cd856c8982b2d194c8a49e4f4c3, v5 (xcart_4_7_7), 2016-12-26 19:10:26, address_buttons.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
<div class="buttons-box">
    <a href="javascript:void(0);" class="update-profile" title="{$lng.lbl_save}" style="display: none;" tabindex="-1">{$lng.lbl_save}</a>
    {* <a href="javascript:void(0);" class="restore-value" title="{$lng.lbl_restore}" style="display: none;" tabindex="-1"></a> *}
    {if $change_mode ne 'Y'}
        <a href="javascript:void(0);" class="edit-profile" title="{$lng.lbl_change}" tabindex="-1"></a>
    {elseif not $active_modules.Bongo_International}
        <a href="javascript:void(0);" class="cancel-edit" title="{$lng.lbl_cancel}" tabindex="-1"></a>
    {/if}
</div>

