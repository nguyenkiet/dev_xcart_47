{*
05e871592394d73776694b9e9c63b82fa8cbb290, v4 (xcart_4_7_7), 2016-07-21 14:15:14, title_selector.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
<select name="{$name|default:"title"}" id="{$id|default:"title"}" {include file="main/attr_orig_data.tpl" data_orig_type=$data_orig_type data_orig_value=$data_orig_value data_orig_keep_empty=$data_orig_keep_empty}>
{if $titles}
{foreach from=$titles item=v}
  <option value="{if $use_title_id eq "Y"}{$v.titleid}{else}{$v.title_orig|escape}{/if}"{if ($use_title_id eq "Y" and $val eq $v.titleid) or ($val|escape eq $v.title_orig|escape)} selected="selected"{/if}>{$v.title}</option>
{/foreach}
{else}
  <option value="{if $use_title_id eq "Y"}{$val}{/if}" selected="selected">{$lng.txt_no_titles_defined}</option>
{/if}
</select>
