{*
44609c4555a246ee0ad2c2f5cc163f05fcc5e473, v5 (xcart_4_7_7), 2016-08-24 11:37:51, config.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{
  "language": "{$shop_language}",
  "theme": "xcart-ui",
  "sorter": "xcartCategoryselectorLowercaseComparison"
}
